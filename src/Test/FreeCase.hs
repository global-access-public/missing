{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE RankNTypes #-}

module Test.FreeCase where

import Control.Monad.Free
import Control.Monad.Trans
import Lib.FreeList
import Pipes.Safe (MonadCatch, SafeT, runSafeT)
import Protolude
import Test.BDD.LanguageFree (FreeBDD)
import Test.Tasty
import Test.Tasty.Bdd (testBehaviorF)
import Test.Tasty.HUnit (Assertion, testCase)

testGroupF :: Text -> Free (FL TestTree) () -> TestTree
testGroupF groupName tests = testGroup (toS groupName) $ freeListOf tests

testCaseF :: MonadFree (FL TestTree) m => TestName -> Assertion -> m ()
testCaseF testName = ele . testCase testName

testBehaviors :: Free (FL TestTree) () -> IO [TestTree]
testBehaviors = pure . freeListOf

bdd :: (MonadFree (FL TestTree) m) => Text -> FreeBDD (SafeT IO) x -> m ()
bdd s x = ele $ testBehaviorF runSafeT (toS s) x

bddState
  :: (MonadFree (FL TestTree) m, Monoid s, Typeable s)
  => Text
  -> FreeBDD (StateT s (SafeT IO)) x
  -> m ()
bddState = bddAny (`evalStateT` mempty)

bddAny
  :: (MonadFree (FL TestTree) m, MonadTrans n, Typeable n, MonadCatch (n (SafeT IO)))
  => (forall a. n (SafeT IO) a -> SafeT IO a)
  -> Text
  -> FreeBDD (n (SafeT IO)) x
  -> m ()
bddAny r s x = ele $ testBehaviorF (runSafeT . r) (toS s) x
