{-# LANGUAGE ImportQualifiedPost #-}
{-# LANGUAGE OverloadedStrings #-}

module System.Cron.Missing
  ( addCronJob
  , execScheduleThrow
  , ScheduleException(..)
  ) where

import System.Cron
    ( CronSchedule(CronSchedule)
    , Job(..)
    , MonadSchedule(..)
    , Schedule
    , ScheduleError(..)
    , forkJob
    , runSchedule
    , scheduleMatches
    )
import Control.Concurrent (ThreadId, forkIO, threadDelay)
import Control.Exception (Exception, throwIO)
import Control.Monad (forever, void, when)
import Data.Text (Text)
import Data.Text qualified as Text
import Data.Time.Clock (UTCTime, getCurrentTime)

addCronJob :: MonadSchedule m => IO () -> CronSchedule -> m ()
addCronJob job schedule = addJob job (text_cronSchedule schedule)

text_cronSchedule :: CronSchedule -> Text
text_cronSchedule (CronSchedule min hour day month dayOfWeek) =
    Text.intercalate " " . map Text.pack $
        show min :
        show hour :
        show day :
        show month :
        show dayOfWeek :
        []

newtype ScheduleException = ScheduleException ScheduleError
  deriving (Show)

instance Exception ScheduleException

-- | Like 'System.Cron.Schedule.execSchedule', but throws an exception when an
-- invalid schedule is passed (instead of printing a notice to stdout and
-- returning @[]@).
execScheduleThrow :: Schedule () -> IO [ThreadId]
execScheduleThrow s = case runSchedule s of
  Left e           -> throwIO (ScheduleException e)
  Right ((), jobs) -> mapM forkJob jobs
