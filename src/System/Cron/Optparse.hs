{-# language ImportQualifiedPost #-}

module System.Cron.Optparse (optSchedule) where


import Options.Applicative
import System.Cron
import Data.Text qualified as Text
import Turtle.Options
import Data.Optional

parseSchedule :: ReadM CronSchedule
parseSchedule = eitherReader (parseCronSchedule . Text.pack)

optSchedule
    :: ArgName
    -> ShortName
    -> Optional HelpMessage
    -> Parser CronSchedule
optSchedule argName c helpMessage
   = option parseSchedule
   $ metavar (Text.unpack (Text.toUpper (getArgName argName)))
  <> long (Text.unpack (getArgName argName))
  <> short c
  <> foldMap (help . Text.unpack . getHelpMessage) helpMessage