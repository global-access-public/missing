{-# LANGUAGE ViewPatterns #-}
{-# LANGUAGE NoImplicitPrelude         #-}
{-# LANGUAGE NoMonomorphismRestriction #-}
{-# LANGUAGE ScopedTypeVariables       #-}
{-# options_ghc -Wall #-}
module Lib.Retry where

import           Control.Monad.Catch
import           Control.Retry
import           Protolude           hiding (mask, try, Handler (..))

-- import Data.Default

def :: Monad m => RetryPolicyM m
def = constantDelay 50000 <> limitRetries 5

rt :: (MonadIO m, MonadCatch m) => m a -> m a
rt = recoverAll'  def . const

recoverAll' :: (MonadIO m, MonadCatch m) => RetryPolicyM m -> (RetryStatus -> m a) -> m a
recoverAll' set = recovering' set handlers
    where
      handlers = skipAsyncExceptions ++ [h]
      h _ = Handler $ \ (_ :: SomeException) -> return True

recovering'
    :: (MonadIO m, MonadCatch m)
    => RetryPolicyM m
    -> [RetryStatus -> Handler m Bool]
    -> (RetryStatus -> m a)
    -> m a
recovering' policy hs f = loop defaultRetryStatus
    where
      loop s = do
        r <- try $ f s
        case r of
          Right x -> return x
          Left e -> recover (e :: SomeException) hs
        where
          recover e [] = throwM e
          recover e ((($ s) -> Handler h) : hs')
            | Just e' <- fromException e = do
                chk <- h e'
                if chk then  do
                    rs <- applyAndDelay policy s
                    case rs of
                      Just rs' -> loop $! rs'
                      Nothing -> throwM e'
                  else throwM e'
            | otherwise = recover e hs'
