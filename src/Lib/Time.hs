{-# LANGUAGE AllowAmbiguousTypes #-}
{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE DerivingStrategies #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TupleSections #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE TypeOperators #-}
{-# OPTIONS_GHC -Wno-orphans #-}

module Lib.Time where

-- (LocalTime, Day, UTCTime, localDay, parseTimeM)

import Control.Lens
    ( (^.),
      view,
      from,
      iso,
      prism',
      re,
      review,
      makeWrapped,
      Field1(_1),
      Field2(_2),
      Getter,
      Iso',
      Lens',
      Prism' )
import Data.Time
    ( fromGregorian,
      toGregorian,
      defaultTimeLocale,
      parseTimeM,
      Day,
      DiffTime,
      UTCTime(UTCTime, utctDay),
      ParseTime,
      LocalTime(LocalTime, localDay),
      TimeOfDay(TimeOfDay) )
import Data.Time.LocalTime.TimeZone.Olson (getTimeZoneSeriesFromOlsonFile)
import Data.Time.LocalTime.TimeZone.Series
  ( TimeZoneSeries
  , localTimeToUTC'
  , utcToLocalTime'
  )
import Data.TreeDiff (ToExpr)
import Data.String ( IsString(..) )
import Protolude hiding (from, to)
import System.IO.Unsafe (unsafePerformIO)

---------------- german time ------------------------

newtype GermanTime = GermanTime {fromGermanTime :: LocalTime} deriving (Show, Eq, Ord)

makeWrapped ''GermanTime

{-# NOINLINE germanTimeSeries #-}
germanTimeSeries :: TimeZoneSeries
germanTimeSeries = unsafePerformIO $ getTimeZoneSeriesFromOlsonFile "/etc/localtime"

utcToGerman :: UTCTime -> GermanTime
utcToGerman = GermanTime . utcToLocalTime' germanTimeSeries

utcFromGerman :: GermanTime -> UTCTime
utcFromGerman (GermanTime lt) = localTimeToUTC' germanTimeSeries lt

isoGerman :: Iso' GermanTime UTCTime
isoGerman = iso utcFromGerman utcToGerman

---------------- month as year + month ------------------------

data Month = Month
  { month_year :: Int
  , month_month :: Int
  }
  deriving (Show, Eq, Ord, ToExpr, Generic)

-- partially broken as it turn years if necessary to accomodate months in 0-11
lensMonth :: Lens' Month Int
lensMonth f (Month y m) = g <$> f m
  where
    g m' =
      let (dy, m'') = divMod (m' - 1) 12
       in Month (y + dy) (m'' + 1)

lensYear :: Lens' Month Int
lensYear f (Month y m) = flip Month m <$> f y

newtype Hour = Hour Int
  deriving newtype (Eq, Ord, Show, Enum, Num)
  deriving (Generic)

makeWrapped ''Hour


instance ToExpr Hour
instance NFData Hour

class DivMod a b where
  type DivModRem a b
  divModIso :: Iso' (a, DivModRem a b) b
  divModZero :: DivModRem a b

-- | DivModIso imply a back lens on the floor
divModLens :: DivMod a b => Lens' b a
divModLens = from divModIso . _1

-- | DivModIso implies a back lens on a reminder,  
-- weak , needs an annotation to resolve 'a'
divModRem :: forall a b. DivMod a b => Lens' b (DivModRem a b)
divModRem = from (divModIso @a) . _2


-- | DivModIso implies a prism based on reminder equality   to its zero
divModPrism :: forall a b. (Eq (DivModRem a b)) => DivMod a b => Prism' b a
divModPrism = prism'
  do \h -> (h, divModZero @a @b) ^. divModIso
  do
    \u ->
      if u ^. divModRem @a == divModZero @a @b
        then Just $ u ^. divModLens
        else Nothing

-- | Promotes a 'a' to a floor in 'b'
divModFloor :: (Eq (DivModRem a b), DivMod a b) =>  Getter a b
divModFloor = re divModPrism 
---------------- month and days ------------------------

instance DivMod Month Day where
  type DivModRem Month Day = Int
  divModIso = iso
    do \(Month y m, d) -> fromGregorian (fromIntegral y) m d
    do
      \day ->
        let (year, month', day') = toGregorian day
         in (Month (fromIntegral year) month', day')
  divModZero = 1

monthOfDay :: Day -> Month
monthOfDay = view divModLens

firstDayOfMonth :: Month -> Day
firstDayOfMonth = review divModPrism

---------------- hour and utc ------------------------
instance DivMod Hour UTCTime where
  type DivModRem Hour UTCTime = DiffTime
  divModIso = iso
    do
      \(Hour h, seconds) ->
        let (days, hours) = divMod h 24
         in UTCTime (toEnum days) $ fromIntegral (hours * 3600) + seconds
    do
      \(UTCTime day seconds) ->
        let (hours, left) = properFraction $ seconds / 3600
         in (Hour $ fromEnum day * 24 + hours, left * 3600)
  divModZero = 0

hourOfUTC :: UTCTime -> Hour
hourOfUTC = view divModLens

hourToUTC :: Hour -> UTCTime
hourToUTC = view divModFloor

---------------- day and utc  ------------------------

instance DivMod Day UTCTime where
  type DivModRem Day UTCTime = DiffTime
  divModIso = iso
    do uncurry UTCTime
    do \(UTCTime day seconds) -> (day, seconds)
  divModZero = 0

---------------- day and germantime ------------------------

instance DivMod Day GermanTime where
  type DivModRem Day GermanTime = TimeOfDay
  divModIso = iso
    do \(day, time) -> GermanTime $ LocalTime day time
    do \(GermanTime (LocalTime day time)) -> (day, time)
  divModZero = TimeOfDay 0 0 0

germanDayOfUTC
  :: UTCTime -- ^ comment
  -> Day
germanDayOfUTC = view $ from isoGerman . divModLens

germanMonthOfUTC :: UTCTime -> Month
germanMonthOfUTC = view $ from isoGerman . divModLens @Day . divModLens

germanDayStart :: Day -> UTCTime
germanDayStart = localTimeToUTC' germanTimeSeries . flip LocalTime (TimeOfDay 0 0 0)

---------------- misc ------------------------

germanHourOfDay :: Day -> Hour
germanHourOfDay = view (divModFloor . isoGerman . divModLens)

germanDayOfHour :: Hour -> Day
germanDayOfHour = view (divModFloor . from isoGerman . divModLens)

monthOfLocalHour :: Hour -> Month
monthOfLocalHour = view (divModFloor . from isoGerman . divModLens @Day . divModLens)    

germanDayAndHourOfUTC :: UTCTime -> (Day, Hour)
germanDayAndHourOfUTC t = (view (from isoGerman . divModLens) t, view divModLens t)


---------------- support for accounting params ------------------------
hourOfGermanTime :: GermanTime -> Hour
hourOfGermanTime = view (isoGerman . divModLens)

---------------- parsing  ------------------------
parseMonth :: Text -> Maybe Month
parseMonth t = monthOfDay <$> parseDay t

parseDay :: Text -> Maybe Day
parseDay = parseTimeM True defaultTimeLocale "%F" . toS

parseDayP :: Prism' Text Day
parseDayP = prism' show parseDay

parseTiming :: forall f a. (ParseTime a, Alternative f, MonadFail f) => [Char] -> f a
parseTiming x =
  p "%Y-%m-%d %H:%M:%S"
    <|> p "%Y-%m-%d %H:%M"
    <|> p "%Y-%m-%d %H"
    <|> p "%Y-%m-%d"
    <|> p "%Y-%m"
    <|> p "%Y"
  where
    p :: [Char] -> f a
    p y = parseTimeM False defaultTimeLocale y x

parseTimeP :: (Show t, ParseTime t) => Prism' Text t
parseTimeP = prism' 
  do show 
  do parseTiming . toS 

instance IsString UTCTime where
  fromString = unsafePerformIO . parseTiming

instance IsString (Maybe GermanTime) where
  fromString = fmap GermanTime . parseTiming

instance IsString GermanTime where
  fromString = GermanTime . unsafePerformIO . parseTiming

instance IsString Day where
  fromString x = utctDay $ fromString x

instance IsString (Maybe Day) where
  fromString x = localDay . fromGermanTime <$> fromString x
