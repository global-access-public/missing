{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE DeriveFunctor #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE PostfixOperators #-}
{-# LANGUAGE TemplateHaskell #-}

module Lib.FreeList where

import qualified Control.Monad.Free as F
import Control.Monad.Free.TH
import Control.Monad.Trans.Free
import qualified Data.Map.Monoidal.Strict as Mm
import qualified Data.Map.Strict as M
import GHC.Exts
import Protolude (sort)

-- | functorial list
--
-- Another way to say [1,2,3]
-- @
--      myList :: [Int]
--      myList = freeListOf $ do
--          fL 1
--          fL 2
--          fL 3
-- @
data FL x a = FL x a
  deriving (Functor)

makeFree ''FL


instance Semigroup (Free (FL x) a) where 
  a <> b = a >> b 

instance Monoid (Free (FL x) ()) where 
  mempty = pure ()
  
-- ele :: a -> Free (FL a) ()
ele :: MonadFree (FL x) m => x -> m ()
ele x = liftF $ FL x ()

(%) :: MonadFree (FL x) m => x -> m ()
(%) = ele

-- | get out a list from the monad description
freeListOf :: F.Free (FL x) a -> [x]
freeListOf (F.Free (FL x g)) = x : freeListOf g
freeListOf (F.Pure _) = []

sortedFreeListOf :: Ord x => F.Free (FL x) a -> [x]
sortedFreeListOf = sort . freeListOf

-- freeTListOf :: FreeT (FL x) a -> [x]
-- freeTListOf (Free (FL x g)) = x : freeListOf g
-- freeTListOf (Pure _       ) = []

freeTListOf :: Monad m => FreeT (FL x) m a -> m [x]
freeTListOf (FreeT g) = do
  r <- g
  case r of
    Free (FL x f) -> (x :) <$> freeTListOf f
    Pure _ -> pure []

fromListF :: IsList l => F.Free (FL (Item l)) () -> l
fromListF = fromList . freeListOf

tuple :: a -> b -> FreeList (a, b)
tuple t s = ele (t, s)

type FreeList a = F.Free (FL a) ()

freeMMap :: (Semigroup a, Ord k) => F.Free (FL (k, a)) () -> Mm.MonoidalMap k a
freeMMap = Mm.fromList . freeListOf

freeMap :: (Ord k) => F.Free (FL (k, a)) () -> M.Map k a
freeMap = M.fromList . freeListOf

makeFList :: [a] -> FreeList a 
makeFList = F.unfold do
  \case
    [] -> Left ()
    (x : xs') -> Right $ FL x xs'
