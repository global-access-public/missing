{-# LANGUAGE RankNTypes #-}

module Data.Logger where

import Protolude

type Logger a = forall m. MonadIO m => a -> m ()
