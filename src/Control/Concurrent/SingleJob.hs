{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE RankNTypes #-}

module Control.Concurrent.SingleJob where

import Control.Concurrent.STM
    ( atomically, newTVarIO, readTVarIO, writeTVar, modifyTVar, readTVar )
import Control.Monad.Catch (finally)
import Data.Logger (Logger)

data SingleJobLog = SingleJobMissed | SingleJobRun Int deriving Show

singleJob :: Logger SingleJobLog -> IO () -> IO (IO ())
singleJob logger f = do
  r <- newTVarIO True
  count <- newTVarIO 0
  pure do
    t <- readTVarIO r
    if t
      then do
        c <- atomically $ do 
          writeTVar r False
          modifyTVar count  succ 
          readTVar count 
        logger $ SingleJobRun c 
        finally f do atomically $ writeTVar r True
      else logger SingleJobMissed
