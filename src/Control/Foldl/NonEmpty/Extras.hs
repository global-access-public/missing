{-# LANGUAGE ImportQualifiedPost #-}
{-# LANGUAGE RankNTypes #-}

module Control.Foldl.NonEmpty.Extras where

import Control.Foldl.NonEmpty (Fold1(Fold1))
import Control.Foldl (Fold(Fold), Handler)
import Prelude
import Control.Foldl qualified as L
import Data.Monoid (Endo(..), Dual (..))
import Data.Functor.Const (Const(..))

_Fold1 :: (a -> a -> a) -> Fold1 a a
_Fold1 f = Fold1 $ \begin -> Fold f begin id

foldMap1 :: Semigroup w => (a -> w) -> (w -> b) -> Fold1 a b
foldMap1 to from = Fold1 $ \begin -> Fold step (to begin) from
  where
    step x y = x <> to y
