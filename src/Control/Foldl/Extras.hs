{-# LANGUAGE ImportQualifiedPost #-}

module Control.Foldl.Extras where

import Control.Foldl qualified as L

premapMaybe :: (a -> Maybe b) -> L.Fold b c -> L.Fold a c
premapMaybe f (L.Fold step begin done) = L.Fold step' begin done
  where
    step' x a = case f a of
      Nothing -> x
      Just b  -> step x b
